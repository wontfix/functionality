package tf.lmao.functionality.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import tf.lmao.functionality.Functionality;

import javax.annotation.Nullable;
import java.util.List;

public class ItemNinjaBlend extends Item {
    public ItemNinjaBlend(String unlocalizedName, String registryName) {
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        tooltip.add("!!! Currently WIP !!! - Will not work");
        tooltip.add("Ninja has noticed your power. He has given this to you as a gift.");
        tooltip.add("Made by smashing (left-click) Gamer Blend against a Gamer Block.");
    }
}

package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.item.ItemSpade;

public class ItemGamerShovel extends ItemSpade {
    public ItemGamerShovel(ToolMaterial material, String unlocalizedName, String registryName) {
        super(material);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
}

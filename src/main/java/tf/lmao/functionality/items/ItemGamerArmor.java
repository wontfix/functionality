package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class ItemGamerArmor extends ItemArmor {
    public ItemGamerArmor(ArmorMaterial material, EntityEquipmentSlot equipmentSlot, String unlocalizedName, String registryName) {
        super(material, 0, equipmentSlot);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
}

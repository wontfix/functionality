package tf.lmao.functionality.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import tf.lmao.functionality.Functionality;
import net.minecraft.item.Item;

import javax.annotation.Nullable;
import java.util.List;

public class ItemGamerBlend extends Item {
    public ItemGamerBlend(String unlocalizedName, String registryName) {
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
    @Override
    public void addInformation(ItemStack item, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        tooltip.add("The forces of diamond and lapis have merged to create a strong material.");
    }
}

package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.item.Item;

public class ItemGamerIngot extends Item {
    public ItemGamerIngot(String unlocalizedName, String registryName) {
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
}

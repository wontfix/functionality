package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.item.ItemAxe;

public class ItemGamerAxe extends ItemAxe {
    public ItemGamerAxe(ToolMaterial material, float damage, float speed, String unlocalizedName, String registryName) {
        super(material, damage, speed);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
    public ItemGamerAxe(ToolMaterial material, String unlocalizedName, String registryName) {
        this(material, 8.0F, 3.1F, unlocalizedName, registryName);
    }
}

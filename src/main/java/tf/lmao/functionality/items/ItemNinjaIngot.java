package tf.lmao.functionality.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import tf.lmao.functionality.Functionality;

import javax.annotation.Nullable;
import java.util.List;

public class ItemNinjaIngot extends Item {
    public ItemNinjaIngot(String unlocalizedName, String registryName) {
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
    @Override
    public void addInformation(ItemStack item, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
       tooltip.add("!!! Currently WIP !!! - Will not work");
       tooltip.add("Throwing the blend into a furnace creates a even stronger material.");
    }
}

package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.item.ItemHoe;

public class ItemGamerHoe extends ItemHoe {
    public ItemGamerHoe(ToolMaterial material, String unlocalizedName, String registryName) {
        super(material);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
}

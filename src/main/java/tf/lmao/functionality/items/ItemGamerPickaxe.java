package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.item.ItemPickaxe;

public class ItemGamerPickaxe extends ItemPickaxe {
    public ItemGamerPickaxe(ToolMaterial material, String unlocalizedName, String registryName) {
        super(material);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
}

package tf.lmao.functionality.items;

import tf.lmao.functionality.Functionality;
import net.minecraft.item.ItemSword;

public class ItemGamerSword extends ItemSword {
    public ItemGamerSword(ToolMaterial material, String unlocalizedName, String registryName) {
        super(material);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
    }
}

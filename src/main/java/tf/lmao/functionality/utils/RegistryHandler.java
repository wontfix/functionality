package tf.lmao.functionality.utils;

import net.minecraft.inventory.EntityEquipmentSlot;
import tf.lmao.functionality.Functionality;
import tf.lmao.functionality.blocks.BasicBlock;
import tf.lmao.functionality.init.ModBlocks;
import tf.lmao.functionality.items.*;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import tf.lmao.functionality.materials.MaterialGamer;

@EventBusSubscriber
public class RegistryHandler {
    @SubscribeEvent
    public static void registerBlocks(Register<Block> event) {
        final Block[] blocks = {
                new BasicBlock(Material.ROCK, "blockGamer", "gamer_block")
        };
        event.getRegistry().registerAll(blocks);
    }
    @SubscribeEvent
    public static void registerItems(Register<Item> event) {
        final Item[] items = {
                new ItemGamerBlend("blendGamer", "gamer_blend"),
                new ItemGamerIngot("ingotGamer", "gamer_ingot"),
                new ItemGamerAxe(MaterialGamer.GAMER_TOOL, "axeGamer", "gamer_axe"),
                new ItemGamerHoe(MaterialGamer.GAMER_TOOL, "hoeGamer", "gamer_hoe"),
                new ItemGamerSword(MaterialGamer.GAMER_TOOL, "swordGamer", "gamer_sword"),
                new ItemGamerShovel(MaterialGamer.GAMER_TOOL, "shovelGamer", "gamer_shovel"),
                new ItemGamerPickaxe(MaterialGamer.GAMER_TOOL, "pickaxeGamer", "gamer_pickaxe"),
                new ItemGamerArmor(MaterialGamer.GAMER_ARMOR, EntityEquipmentSlot.HEAD, "helmetGamer", "gamer_helmet"),
                new ItemGamerArmor(MaterialGamer.GAMER_ARMOR, EntityEquipmentSlot.CHEST, "chestplateGamer", "gamer_chestplate"),
                new ItemGamerArmor(MaterialGamer.GAMER_ARMOR, EntityEquipmentSlot.LEGS, "leggingsGamer", "gamer_leggings"),
                new ItemGamerArmor(MaterialGamer.GAMER_ARMOR, EntityEquipmentSlot.FEET, "bootsGamer", "gamer_boots"),
                new ItemNinjaBlend("blendNinja", "ninja_blend"),
                new ItemNinjaIngot("ingotNinja", "ninja_ingot")
        };

        final Item[] itemBlocks = {
                new ItemBlock(ModBlocks.GAMER_BLOCK).setRegistryName(ModBlocks.GAMER_BLOCK.getRegistryName())
        };

        event.getRegistry().registerAll(items);
        event.getRegistry().registerAll(itemBlocks);
    }
}

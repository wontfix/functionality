package tf.lmao.functionality.utils;

import tf.lmao.functionality.init.ModItems;
import tf.lmao.functionality.init.ModBlocks;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@EventBusSubscriber(Side.CLIENT)
public class ModelRegistryHandler {
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        registerModel(ModItems.GAMER_BLEND);
        registerModel(ModItems.GAMER_INGOT);

        registerModel(ModItems.GAMER_AXE);
        registerModel(ModItems.GAMER_HOE);
        registerModel(ModItems.GAMER_PICKAXE);
        registerModel(ModItems.GAMER_SHOVEL);
        registerModel(ModItems.GAMER_SWORD);

        registerModel(ModItems.GAMER_HELMET);
        registerModel(ModItems.GAMER_CHESTPLATE);
        registerModel(ModItems.GAMER_LEGGINGS);
        registerModel(ModItems.GAMER_BOOTS);

        registerModel(ModItems.NINJA_BLEND);
        registerModel(ModItems.NINJA_INGOT);

        registerModel(Item.getItemFromBlock(ModBlocks.GAMER_BLOCK));
    }

    private static void registerModel(Item item) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
}

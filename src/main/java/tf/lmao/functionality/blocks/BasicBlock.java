package tf.lmao.functionality.blocks;

import tf.lmao.functionality.Functionality;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BasicBlock extends Block {
    public BasicBlock(Material material, String unlocalizedName, String registryName) {
        this(material, SoundType.STONE, unlocalizedName, registryName);
    }
    public BasicBlock(Material material, SoundType sound, String unlocalizedName, String registryName) {
        super(material);
        setTranslationKey(Functionality.MOD_ID + "." + unlocalizedName);
        setRegistryName(registryName);
        setCreativeTab(Functionality.CREATIVE_TAB);
        setSoundType(sound);
    }
}

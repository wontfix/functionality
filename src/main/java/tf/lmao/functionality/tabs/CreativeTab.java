package tf.lmao.functionality.tabs;

import tf.lmao.functionality.Functionality;
import tf.lmao.functionality.init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CreativeTab extends CreativeTabs {
    public CreativeTab(String name) {
        super(Functionality.MOD_ID + "." + name);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public ItemStack createIcon() {
        return new ItemStack(ModItems.GAMER_INGOT);
    }
}

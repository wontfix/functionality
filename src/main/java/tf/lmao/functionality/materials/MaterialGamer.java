package tf.lmao.functionality.materials;

import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import tf.lmao.functionality.Functionality;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class MaterialGamer {
    public static final ToolMaterial GAMER_TOOL = EnumHelper.addToolMaterial(Functionality.MOD_ID + ":gamer_tool", 4, 6969, 4.2F, 6.9F, 10);
    public static final ArmorMaterial GAMER_ARMOR = EnumHelper.addArmorMaterial(Functionality.MOD_ID + ":gamer_armor", Functionality.MOD_ID + ":gamer", 6969, new int[]{4, 8, 9, 4}, 10, SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, 3);
}

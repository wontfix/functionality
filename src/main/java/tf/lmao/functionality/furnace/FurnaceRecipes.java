package tf.lmao.functionality.furnace;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import tf.lmao.functionality.init.ModItems;

public class FurnaceRecipes {
    public static void initRecipes() {
        GameRegistry.addSmelting(ModItems.GAMER_BLEND, new ItemStack(ModItems.GAMER_INGOT), 10.0F);
    }
}

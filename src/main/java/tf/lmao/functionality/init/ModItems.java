package tf.lmao.functionality.init;
import tf.lmao.functionality.Functionality;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

@ObjectHolder(Functionality.MOD_ID)
public class ModItems {
    public static final Item GAMER_BLEND = null;
    public static final Item GAMER_INGOT = null;

    public static final Item GAMER_AXE = null;
    public static final Item GAMER_SWORD = null;
    public static final Item GAMER_PICKAXE = null;
    public static final Item GAMER_SHOVEL = null;
    public static final Item GAMER_HOE = null;

    public static final Item GAMER_HELMET = null;
    public static final Item GAMER_CHESTPLATE = null;
    public static final Item GAMER_LEGGINGS = null;
    public static final Item GAMER_BOOTS = null;

    public static final Item NINJA_BLEND = null;
    public static final Item NINJA_INGOT = null;
}
